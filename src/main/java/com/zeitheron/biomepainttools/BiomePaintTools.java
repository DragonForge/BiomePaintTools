package com.zeitheron.biomepainttools;

import com.zeitheron.biomepainttools.items.ItemBiomePencil;
import com.zeitheron.biomepainttools.proxy.CommonProxy;
import com.zeitheron.hammercore.internal.SimpleRegistration;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistry;

@Mod(modid = "biomepainttools", name = "Biome Paint Tools", version = "@VERSION@", dependencies = "required-after:hammercore")
public class BiomePaintTools
{
	public static final ItemBiomePencil pencil = new ItemBiomePencil();
	
	@Instance("biomepainttools")
	public static BiomePaintTools instance;
	
	@SidedProxy(modId = "biomepainttools", clientSide = "com.zeitheron.biomepainttools.proxy.ClientProxy", serverSide = "com.zeitheron.biomepainttools.proxy.CommonProxy")
	public static CommonProxy proxy;
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent e)
	{
		SimpleRegistration.registerItem(pencil, "biomepainttools", CreativeTabs.MISC);
	}
	
	@EventHandler
	public void init(FMLInitializationEvent e)
	{
		proxy.init();
	}
	
	@SubscribeEvent
	public void addRecipes(RegistryEvent.Register<IRecipe> recipe)
	{
		IForgeRegistry<IRecipe> recipes = recipe.getRegistry();
		
		recipes.register(SimpleRegistration.parseShapedRecipe(pencil.DEFINST(), "eg ", "gig", " gi", 'e', "gemEmerald", 'g', "ingotGold", 'i', "ingotIron").setRegistryName("craft_pencil"));
		recipes.register(SimpleRegistration.parseShapelessRecipe(pencil.DEFINST(), new ItemStack(pencil, 1, OreDictionary.WILDCARD_VALUE)).setRegistryName("reset_pencil"));
	}
}