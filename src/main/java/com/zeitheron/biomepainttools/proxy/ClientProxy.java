package com.zeitheron.biomepainttools.proxy;

import com.zeitheron.biomepainttools.BiomePaintTools;
import com.zeitheron.biomepainttools.items.ItemBiomePencil;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHandSide;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ClientProxy extends CommonProxy
{
	@Override
	public void init()
	{
		Minecraft.getMinecraft().getItemColors().registerItemColorHandler((stack, layer) ->
		{
			if(layer == 1)
			{
				Biome biome = ItemBiomePencil.getCurrentBiome(stack);
				if(biome != null)
					return biome.getGrassColorAtPos(Minecraft.getMinecraft().player.getPosition());
			}
			
			return 0xFFFFFF;
		}, BiomePaintTools.pencil);
		
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	@SubscribeEvent
	public void renderHUD(RenderGameOverlayEvent.Post e)
	{
		if(e.getType() == ElementType.ALL)
		{
			FontRenderer f = Minecraft.getMinecraft().fontRenderer;
			ItemStack mainhand = Minecraft.getMinecraft().player.getHeldItemMainhand();
			ItemStack offhand = Minecraft.getMinecraft().player.getHeldItemOffhand();
			EnumHandSide handMain = Minecraft.getMinecraft().gameSettings.mainHand;
			
			if(!mainhand.isEmpty() && mainhand.getItem() == BiomePaintTools.pencil && ItemBiomePencil.hasCurrentBiome(mainhand) && mainhand.getItemDamage() < BiomePaintTools.pencil.getMaxDamage(mainhand))
			{
				String bn = I18n.format("hud.biomepainttools:biome_name") + ": ";
				Biome b = ItemBiomePencil.getCurrentBiome(mainhand);
				bn += I18n.format(b != null ? b.getBiomeName() : "biome.unknown");
				float x = handMain == EnumHandSide.LEFT ? 2F : (float) e.getResolution().getScaledWidth_double() - f.getStringWidth(bn) - 2F;
				f.drawString(bn, x, (float) e.getResolution().getScaledHeight_double() / 2F - 14F, 0xFFFFFF, false);
			}
			
			if(!offhand.isEmpty() && offhand.getItem() == BiomePaintTools.pencil && ItemBiomePencil.hasCurrentBiome(offhand) && offhand.getItemDamage() < BiomePaintTools.pencil.getMaxDamage(offhand))
			{
				String bn = I18n.format("hud.biomepainttools:biome_name") + ": ";
				Biome b = ItemBiomePencil.getCurrentBiome(offhand);
				bn += I18n.format(b != null ? b.getBiomeName() : "biome.unknown");
				float x = handMain == EnumHandSide.LEFT ? (float) e.getResolution().getScaledWidth_double() - f.getStringWidth(bn) - 2F : 2F;
				f.drawString(bn, x, (float) e.getResolution().getScaledHeight_double() / 2F - 14F, 0xFFFFFF, false);
			}
		}
	}
}