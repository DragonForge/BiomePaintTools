package com.zeitheron.biomepainttools.items;

import java.util.List;

import javax.annotation.Nullable;

import org.lwjgl.input.Keyboard;

import com.zeitheron.biomepainttools.BiomePaintTools;
import com.zeitheron.hammercore.HammerCore;
import com.zeitheron.hammercore.utils.NBTUtils;
import com.zeitheron.hammercore.utils.WorldLocation;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBiomePencil extends Item
{
	public ItemBiomePencil()
	{
		setUnlocalizedName("biome_pencil");
		setMaxDamage(256);
		setMaxStackSize(1);
	}
	
	public ItemStack DEFINST()
	{
		return new ItemStack(this, 1, 256);
	}
	
	public ItemStack DEFChINST()
	{
		return new ItemStack(this, 1, 0);
	}
	
	@Override
	public ItemStack getDefaultInstance()
	{
		return DEFINST();
	}
	
	@Override
	public void getSubItems(CreativeTabs c, NonNullList<ItemStack> l)
	{
		if(!isInCreativeTab(c))
			return;
		l.add(new ItemStack(this, 1, 256));
		ForgeRegistries.BIOMES.getValuesCollection().stream().map(biome -> setCurrentBiome(DEFChINST(), biome)).forEach(l::add);
	}
	
	@Override
	public int getRGBDurabilityForDisplay(ItemStack stack)
	{
		NBTTagCompound tag = stack.getTagCompound();
		if(tag != null)
		{
			Biome biome = getCurrentBiome(stack);
			if(biome != null)
			{
				float use = 1F - (float) getDurabilityForDisplay(stack);
				int color = biome.getGrassColorAtPos(Minecraft.getMinecraft().player.getPosition());
				return MathHelper.multiplyColor(color, MathHelper.rgb(use, use, use));
			}
		}
		
		return super.getRGBDurabilityForDisplay(stack);
	}
	
	@Override
	public boolean isRepairable()
	{
		return false;
	}
	
	@Override
	public EnumActionResult onItemUse(EntityPlayer p, World w, BlockPos pos, EnumHand hand, EnumFacing side, float p_onItemUse_7_, float p_onItemUse_8_, float p_onItemUse_9_)
	{
		ItemStack stack = p.getHeldItem(hand);
		
		if(!stack.hasTagCompound())
			stack.setTagCompound(new NBTTagCompound());
		
		int x = Math.abs(pos.getX() < 0 ? (16 + pos.getX()) % 16 : pos.getX() % 16) % 16;
		int z = Math.abs(pos.getZ() < 0 ? (16 + pos.getZ()) % 16 : pos.getZ() % 16) % 16;
		
		Biome biome = w.getBiome(pos);
		
		if(p.isSneaking())
		{
			if(!hasCurrentBiome(stack) || stack.getItemDamage() == getMaxDamage(stack))
			{
				setCurrentBiome(stack, biome);
				if(!w.isRemote)
					HammerCore.audioProxy.playSoundAt(w, "biomepainttools:biome_absorb", pos, 1F, 1F, SoundCategory.PLAYERS);
				if(p.capabilities.isCreativeMode)
				{
					stack.setItemDamage(0);
					p.setHeldItem(hand, stack.copy());
				} else
					stack.setItemDamage(stack.getItemDamage() - 1);
			} else if(getCurrentBiome(stack) == biome && stack.getItemDamage() > 0)
			{
				if(p.capabilities.isCreativeMode)
				{
					stack.setItemDamage(0);
					p.setHeldItem(hand, stack.copy());
				} else
					stack.setItemDamage(stack.getItemDamage() - 1);
				if(!w.isRemote)
					HammerCore.audioProxy.playSoundAt(w, "biomepainttools:biome_absorb", pos, 1F, 1F, SoundCategory.PLAYERS);
			}
		} else if(hasCurrentBiome(stack) && getCurrentBiome(stack) != biome && stack.getItemDamage() < getMaxDamage(stack))
		{
			stack.setItemDamage(stack.getItemDamage() + 1);
			
			new WorldLocation(w, pos).setBiome(getCurrentBiome(stack));
			w.markAndNotifyBlock(pos, w.getChunkFromBlockCoords(pos), w.getBlockState(pos), w.getBlockState(pos), 3);
			
			if(!w.isRemote)
				HammerCore.audioProxy.playSoundAt(w, "biomepainttools:biome_spread", pos, 1F, 1F, SoundCategory.PLAYERS);
		}
		
		return EnumActionResult.PASS;
	}
	
	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack i, World worldIn, List<String> list, ITooltipFlag p_addInformation_4_)
	{
		if(!i.isEmpty() && hasCurrentBiome(i) && i.getItemDamage() < BiomePaintTools.pencil.getMaxDamage())
		{
			String bn = I18n.format("hud.biomepainttools:biome_name") + ": ";
			Biome b = getCurrentBiome(i);
			bn += I18n.format(b != null ? b.getBiomeName() : "biome.unknown");
			list.add(bn + " (" + Biome.getIdForBiome(b) + ")");
		}
		
		if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT))
		{
			if(!i.isEmpty() && hasCurrentBiome(i) && i.getItemDamage() < getMaxDamage(i))
			{
				Biome b = getCurrentBiome(i);
				String bn = I18n.format(b != null ? b.getBiomeName() : "biome.unknown");
				list.add(I18n.format("info.biomepainttools:bound", bn));
				list.add(I18n.format("info.biomepainttools:chinf_1", getMaxDamage(i) - i.getItemDamage()));
				list.add(I18n.format("info.biomepainttools:chinf_2"));
				list.add(I18n.format("info.biomepainttools:glinf_3"));
				list.add(I18n.format("info.biomepainttools:glinf_4"));
			} else
			{
				list.add(I18n.format("info.biomepainttools:dpinf_1"));
				list.add(I18n.format("info.biomepainttools:dpinf_2"));
				list.add(I18n.format("info.biomepainttools:glinf_3"));
				list.add(I18n.format("info.biomepainttools:glinf_4"));
			}
		} else
			list.add(I18n.format("info.biomepainttools:prinf", TextFormatting.YELLOW + Keyboard.getKeyName(Keyboard.KEY_RSHIFT).substring(1) + TextFormatting.GRAY));
	}
	
	public static boolean hasCurrentBiome(ItemStack stack)
	{
		return getCurrentBiome(stack) != null;
	}
	
	@Nullable
	public static Biome getCurrentBiome(ItemStack stack)
	{
		if(stack.hasTagCompound() && stack.getTagCompound().hasKey("CurrentBiome", NBT.TAG_STRING))
			return ForgeRegistries.BIOMES.getValue(new ResourceLocation(stack.getTagCompound().getString("CurrentBiome")));
		return null;
	}
	
	public static ItemStack setCurrentBiome(ItemStack stack, Biome biome)
	{
		if(!stack.hasTagCompound())
			stack.setTagCompound(new NBTTagCompound());
		if(biome != null)
			stack.getTagCompound().setString("CurrentBiome", biome.getRegistryName().toString());
		else
			NBTUtils.removeTagFromItemStack(stack, "CurrentBiome");
		return stack;
	}
}